# Eksāmens Teletrafika Teorijā

#### Date: January 4, 2018

## Studentu saraksts konsultācijā:
* Laura Skladova 141REB201 [1.dala](https://bitbucket.org/laurateam/laura/src/ff73c645013345cee4cb1a138f3834cbb2186826/?at=End)[2.dala](https://bitbucket.org/laurateam/laura/commits/a5f6a3ccb5f3bdd3a132ed0e156a97b349b8c18f?at=master)
* Melānija Ivulāne 141REB126 [1.dala](https://bitbucket.org/laurateam/laura/src/ff73c645013345cee4cb1a138f3834cbb2186826/?at=End)[2.dala](https://bitbucket.org/laurateam/laura/commits/a5f6a3ccb5f3bdd3a132ed0e156a97b349b8c18f?at=master)
* Anna Stankeviča 111REB721 [1.dala](https://bitbucket.org/laurateam/laura/src/ff73c645013345cee4cb1a138f3834cbb2186826/?at=End)[2.dala](https://bitbucket.org/laurateam/laura/commits/a5f6a3ccb5f3bdd3a132ed0e156a97b349b8c18f?at=master)
* Vita Šmidberga 141REB141 https://bitbucket.org/VitaSmidberga/rae555
* Koņevs Jevgēnijs 141RMC189 https://bitbucket.org/JevgenijsRAE555/rae555
* Griblo Toms 141REB139 https://bitbucket.org/TomsGriblo/rae555
* Šķiliņš Roberts 141REB148 https://bitbucket.org/Roberts_Skilins/rae555
* Vertašonoka Anastasija 141REB160 https://bitbucket.org/Ananas999/rae555

#### Date: January 5, 2018

## Studentu saraksts konsultācijā:
* Oskars Suheckis 131REB232 https://bitbucket.org/Ooskars/repo-exam-rae555-lv
* Kaspars Zaķis 141REB125 https://bitbucket.org/kaspars_zakis/rae555
* Viktors Ananičs 141REB137 https://bitbucket.org/VVVVictor/repo-exam-rae555-1718-lv/wiki/Home
* Andrejs Pastuhovs 141REB127 https://bitbucket.org/AndrejsPastuhovs/repo-exam-rae555-1718-lv
* Māris Bokta 141REB124 https://Maris_Bokta@bitbucket.org/Maris_Bokta/rae555
----------------------

## Exāmena uzdevums 
* Savā REPO (RAE555) iestatījumos ieslēgt **WIKI**
* Reģistrēties SAFARIBOOKSONLINE
* Atrast video kursu: [Mastering Data Analysis with R](https://www.safaribooksonline.com/library/view/learning-path-r/9781788470681/)
	* MAXIMĀLI atkārtot nodaļu [Chapter 1 : Learning Data Analysis with R](https://www.safaribooksonline.com/library/view/learning-path-r/9781788470681/video1_1.html)
	* Par pamatu datu izgūšanai kalpo: [Downloading Open Data from FTP Sites](https://www.safaribooksonline.com/library/view/learning-path-r/9781788470681/video1_3.html)
	* BET! jūsu dati tiek ņemti no jebkura cita FTP vai HTTP datu plūsmas avota! 
	* Sakritība starp studentu variantiem ir jāizslēdz!
* Askaiti par VIDEO KURSĀ piedāvāto datu analīzi publicējam Jūsu PERSONĪGAjĀ REPO, WIKI sadaļā.
* Linku uz Jūsu PERSONĪGAjĀ REPO WIKI sadaļu publicējiet jūsu PERSONĪGAjĀ REPO, **Readme.md** dokumentā


### Useful link
[How to markdown -- demo](https://bitbucket.org/tutorials/markdowndemo)

### Deadline to finish Course with 4
* 22:00 EET, Friday, January 5, 2018
* At this time, You should succeed with the Report on Your WIKI:
	* Unique FTP or HTTP Data Source publishing (please, avoid coincidences)
	* [Cleaning Your Data](https://www.safaribooksonline.com/library/view/learning-path-r/9781788470681/video1_6.html)

### Deadline to finish Course
* 24:00 EET, Monday, January 8, 2018
* At this time, snapshots of Your REPOs will be gathered by staff and scored.


